//package
package com.bootcamp.dicoveri.Eurofin;

//imports
import java.util.Scanner;

class PrintSequence{
	//Implementation of main method
	public static void main(String... args){
		//Program to print following series: 1,4,9,25,36,49,81,100.....N
		//Declaration
		int n;
		
		//Scanner class object creation
		Scanner sc = new Scanner(System.in);
		
		//Accept the n-th value from console input
		System.out.println("Enter the n-th value : ");
		n = sc.nextInt();
		
		//method call for display series
		display(n);
	}
	
	//Method to display series
	public static void display(int n){
		for(int i=1;i<=n;i++){
			if(i%4==0){
				continue;
			}
			else{
				System.out.print((i*i));
			}
			if(i<n){
				System.out.print(",");
			}
		}
	}
}