//package
package com.bootcamp.dicoveri.Eurofin;

//imports
import java.util.Scanner;

class SumOfPrimeNumbers{
	//Method to return true if number is prime
	static boolean isPrime(int number){
		boolean flag = true;
		for(int i=2;i<number/2;i++){
			if(number%i==0){
				flag = false;
				break;
			}
		}
		return flag;
	}
	
	//Method to Caculate Sum of Prime numbers
	public static int sumOfPrimeNumbers(int lowerRange, int upperRange){
		//Declaration with initial value
		int sum = 0;
		
		//display prime numbers
		System.out.print("Prime Numbers :");
		for(int i=lowerRange;i<=upperRange;i++){
			if(isPrime(i)){
				sum += i;
				System.out.print(" "+i);
			}
		}
		
		//Return
		return sum;
	}
	
	//Implementation of main method
	static public void main(String[] args){
		//Program to caclulate prime numbers with sum of those numbers
		//Declarations
		int lowerRange, upperRange, sum;
		
		//Initialization
		sum = 0;
		
		//Create object for Scanner
		Scanner sc = new Scanner(System.in);
		
		//Accept the ranges between primes numbers
		System.out.println("Enter the range between N and M : ");
		lowerRange = sc.nextInt();
		upperRange = sc.nextInt();
		
		//Method call to find sum of primes
		sum = sumOfPrimeNumbers(lowerRange, upperRange);
		
		//Display sum of primes
		System.out.println("\nSum of Odd Number between "+lowerRange+" and "+upperRange+" : "+sum);
	}
}