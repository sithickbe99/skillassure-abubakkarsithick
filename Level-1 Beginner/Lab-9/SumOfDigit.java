//package
package com.bootcamp.dicoveri.Eurofin;

//imports
import java.util.Scanner;

class SumOfDigit{
	//Method return an sum of each digit
	static public int reverse(int number){
		//Declaration with initial value
		int revs = 0;
		
		while(number>0){
			revs = revs + number % 10;
			number /= 10;
		}
		return revs;
	}
	
	//implementation of main method
	static public void main(String[] args){
		//Program to calculate sum of  an given number
		//Declaration
		int number,reversedNumber;
		
		//Accept and Store an number to variable
		Scanner sc = new Scanner(System.in);
		number = sc.nextInt();
		
		//Method call to get sum of an number
		reversedNumber = reverse(number);
		
		//Display the result
		System.out.println(reversedNumber);
	}
}