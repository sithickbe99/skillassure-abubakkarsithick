//package
package com.bootcamp.dicoveri.Eurofin;

//imports
import java.util.Scanner;

class ViceVersaCaseChanges{
	//Implementation of main method
	static public void main(String[] args){
		//Program to change an vice versa case for given sentence.
		//Declaration
		String sentence,resultStr;
		
		//Intialization
		resultStr = "";
		
		//Accept the value from console
		Scanner sc = new Scanner(System.in);
		sentence = sc.nextLine();
		
		//implementation of Letter case changes
		for(int i=0;i<sentence.length();i++){
			if(sentence.charAt(i)>='a' && sentence.charAt(i)<='z'){
				resultStr += (char)(((int)sentence.charAt(i))-32);
			}else if(sentence.charAt(i)>='A' && sentence.charAt(i)<='Z'){
				resultStr += (char)(((int)sentence.charAt(i))+32);
			}else{
				resultStr += sentence.charAt(i);
			}
		}
		
		//store result in resultStr variable and displayed here.
		System.out.println(resultStr);
	}
}