//package
package com.bootcamp.dicoveri.Eurofin;

//imports
import java.util.Scanner;

class ClaculateSimpleInterest{
	//Implementation of main method
	public static void main(String... args){
		//Program to calculate simple interest
		//Declaration
		int principle;
		double rateOfInterest,time,simpleInterest;
		
		//Initialization
		principle = 0;
		rateOfInterest = 0.0;
		time = 0.0;
		simpleInterest = 0.0;
		
		//Scanner class objecct
		Scanner sc = new Scanner(System.in);
		
		//Accept the principle,rateOfInterest and time duration
		System.out.println("principle: ");
		principle = sc.nextInt();
		System.out.println("rateOfInterest: ");
		rateOfInterest = sc.nextDouble();
		System.out.println("time: ");
		time = sc.nextDouble();
		
		//method call to calculate S.I.
		simpleInterest = calculateSimpleInterest(principle,rateOfInterest,time);
		
		//Display Simple Interest
		System.out.println("Simple Interest: "+simpleInterest);
	}
	
	//method implementation for calculate simple interest
	public static double calculateSimpleInterest(int principle,double rateOfInterest,double time){
		//Return
		return (principle*rateOfInterest*time)/100;
	}
	
}