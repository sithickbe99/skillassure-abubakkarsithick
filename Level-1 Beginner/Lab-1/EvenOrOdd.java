//package
package com.bootcamp.dicoveri.Eurofin;

//imports
import java.util.Scanner;

class EvenOrOdd{
	//Implementation of main method
	static public void main(String... args){
		//Program to find whether given number odd or even.
		//Declaration
		int number;
		
		//Scanner class object creation
		Scanner sc = new Scanner(System.in); 
		
		//Accept the number
		System.out.println("Enter the Number : ");
		number = sc.nextInt();
		
		//method call to find odd or even
		if(number >= 0){
			if(isOddNumber(number)){
				System.out.println("ODD");
			}else{
				System.out.println("EVEN");
			}
		}else{
			System.out.println("Negative Number");
		}
	}
	
	//Method to return true if number is odd otherwise false
	public static boolean isOddNumber(int number){
		//Return
		return number%2==1;
	}
}