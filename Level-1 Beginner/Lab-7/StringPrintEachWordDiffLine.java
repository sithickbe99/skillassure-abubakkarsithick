//package
package com.bootcamp.dicoveri.Eurofin;

//imports
import java.util.Scanner;


class StringPrintEachWordDiffLine{
	//Implementation of main method
	static public void main(String[] args){
		//Program to print words line by line.
		//Declaration
		String words[], sentence;
		
		//Scanner object
		Scanner sc = new Scanner(System.in);
		
		//Accept the String value
		sentence = sc.nextLine();
		
		//Split into an separate words to store
		words = sentence.trim().split(" ");
		
		//Display words line by line
		for(String s : words){
			System.out.println(s);
		}
	}
}