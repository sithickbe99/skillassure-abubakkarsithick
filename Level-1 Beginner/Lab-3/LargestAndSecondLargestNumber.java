//package
package com.bootcamp.dicoveri.Eurofin;

//imports
import java.util.Scanner;

class LargestAndSecondLargestNumber{
	//Approach-I
	/*static int maxNumber(int number1, int number2){
		if(number1>number2){
			return number1;
		}
		return number2;
	}*/
	
	//Implementation of main method
	static public void main(String... args){
		//Program to find Largest and Second Largest number
		//Declaration
		int firstNumber,secondNumber,thirdNumber,largest,secondLargest;
		
		//Scanner class object creation
		Scanner sc = new Scanner(System.in); 
		
		//Accept the 3 numbers
		System.out.println("Enter the three Numbers : ");
		firstNumber = sc.nextInt();
		secondNumber = sc.nextInt();
		thirdNumber = sc.nextInt();
		
		//Implementation for to find Largest and Second Largest number
		if(firstNumber>secondNumber && firstNumber>thirdNumber){
			largest = firstNumber;
			
			//Approach-I
			/*if(secondNumber>thirdNumber){
				secondLargest = secondNumber;
			}else{
				secondLargest = thirdNumber;
			}*/
			//secondLargest = maxNumber(secondNumber,thirdNumber);
			
			//Approach-II
			secondLargest = Math.max(secondNumber,thirdNumber);
		}else if(secondNumber>firstNumber && secondNumber>thirdNumber){
			largest = secondNumber;
			
			//Approach-I
			/*if(firstNumber>thirdNumber){
				secondLargest = firstNumber;
			}else{
				secondLargest = thirdNumber;
			}*/
			//secondLargest = maxNumber(firstNumber,thirdNumber);
			
			//Approach-II
			secondLargest = Math.max(firstNumber,thirdNumber);
		}else {
			largest = thirdNumber;
			
			//Approach-I
			/*if(firstNumber>secondNumber){
				secondLargest = firstNumber;
			}else{
				secondLargest = secondNumber;
			}*/
			//secondLargest = maxNumber(firstNumber,secondNumber);
			
			//Approach-II
			secondLargest = Math.max(firstNumber,secondNumber);
		}
		
		//Display the result
		System.out.println("Largest : "+largest+" & Second Largest : "+secondLargest);
	}
}