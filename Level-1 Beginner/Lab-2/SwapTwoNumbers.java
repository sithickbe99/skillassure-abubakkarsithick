//package
package com.bootcamp.dicoveri.Eurofin;

//imports
import java.util.Scanner;

class SwapTwoNumbers{
	//Implementation of main method
	static public void main(String... args){
		//Program to swap two numbers
		//Declaration
		int firstNumber,secondNumber,temp;
		
		//Scanner class object
		Scanner sc = new Scanner(System.in); 
		
		//Accept two numbers
		System.out.println("Enter the first Number : ");
		firstNumber = sc.nextInt();
		System.out.println("Enter the second Number : ");
		secondNumber = sc.nextInt();
		
		//display the two number before swapping
		System.out.println("Before Swapping : "+firstNumber+" "+secondNumber);
		
		//swapping of two numbers using temp variable
		int temp = firstNumber;
		firstNumber = secondNumber;
		secondNumber = temp;
		
		//display the two numbers after swapping
		System.out.println("After Swapping : "+firstNumber+" "+secondNumber);
	}
}