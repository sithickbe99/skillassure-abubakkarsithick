//package
package com.bootcamp.dicoveri.Eurofin;

//imports
import java.util.Scanner;

class SumOfOddNumbers{
	//Implementation of main method
	static public void main(String[] args){
		//Program to compute sum of odd number
		//Declaration
		int range,sum;
		
		//Intialization
		sum = 0;
		
		//Scanner Class object
		Scanner sc = new Scanner(System.in);
		
		//Accept the range value
		System.out.println("Enter the range of Value N : ");
		range = sc.nextInt();
		
		//Calculation
		for(int i=1;i<=range;i++){
			if(i%2==1){
				sum += i;
			}
		}
		
		//Display result
		System.out.println("The Sum of Odd Number between 1 and "+range+" : "+sum);
	}
}