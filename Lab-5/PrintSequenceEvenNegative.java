//package
package com.bootcamp.dicoveri.Eurofin;

//imports
import java.util.Scanner;

class PrintSequenceEvenNegative{
	//Implementations of main method
	public static void main(String... args){
		//Program to print following series: 1,-2,3,-4,5,-6,.....N
		//Declaration
		int n;
		
		//Scanner class object
		Scanner sc = new Scanner(System.in);
		
		//Accept the n-th value
		System.out.println("Enter the n-th value : ");
		n = sc.nextInt();
		
		//method call for print series
		display(n);
	}
	
	//method implementation to display series
	public static void display(int n){
		for(int i=1;i<=n;i++){
			if(i%2==0){
				System.out.print(i*-1);
			}
			else{
				System.out.print(i);
			}
			if(i<n){
				System.out.print(",");
			}
		}
	}
}