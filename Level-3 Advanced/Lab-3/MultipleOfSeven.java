//package
package com.bootcamp.dicoveri.Eurofin;

class MultipleOfSeven{
	//Implementation of main method
	public static void main(String[] args){
		//Declaration
		int arr[],number,flag;
		
		//Initialization
		arr = new int[25];
		number = 7;
		flag = 0;
		
		//Filter the multiples of 7 and get remainder 1 when divide by 2,3,4,5,6.
		System.out.println("The 25 elements are:");
		while(flag < 25){
			if(number%7 == 0 && number%2 == 1 && number%3 == 1 && number%4 == 1 && number%5 == 1 && number%6 == 1){
				System.out.println(number);
				arr[flag] = number;
				flag += 1;
			}
			number++;
		}
		
		//Display result
		display(arr);	
	}
	
	//Method to display the multiples ends with 01
	public static void display(int[] arr){
		//Declaration and Initialization
		int multiplesCount = 0;
		
		//Display the multiples ends with 01
		System.out.println("List of multiples that ends with 01 are :");
		for(int a : arr){
			String value = String.valueOf(a);
			if(value.substring(value.length()-2).equals("01")){
				System.out.println(a);
				multiplesCount++;
			}
		}
		
		//Display the count of multiples ends with 01
		System.out.println("Count of multiples ends with 01: "+multiplesCount);
	}
}