//package
package com.bootcamp.dicoveri.Eurofin;

//imports
import java.util.Scanner;

//Implementation of Half Pyramid class
class TransposeMatrix{
	//Implementation of main method
	public static void main(String[] args){
		//Program to transpose an given matrix
		//Declaration
		int row,col;

		//Scanner class object creation
		Scanner sc = new Scanner(System.in);
		
		//Accept rows and columns
		row = sc.nextInt();
		col = sc.nextInt();
		
		//Method call for display inital Matrix
		display(initialMatrix(row,col));
		
		//New Line
		System.out.println();
		
		//Method call for transpose an matrix and display
		display(transposeMatrix(initialMatrix(row,col),col,row));
	}
	
	//Method implementation for Matrix transpose
	public static int[][] transposeMatrix(int[][] matrix,int row,int col){
		//Declaration and Initialization
		int[][] transposeMatrix = new int[row][col];
		
		//transpose matrix elements to MxN Matrix
		for(int i=0;i<row;i++){
			for(int j=0;j<col;j++){
				transposeMatrix[i][j] = matrix[j][i];
			}
		}
		
		//return
		return transposeMatrix;
	}
	
	//Method for display Matrix
	public static void display(int[][] matrix){
		for(int[] outer : matrix){
			for(int inner : outer){
				System.out.print(inner+" ");
			}
			//New Line
			System.out.println();
		}
	}
	
	//Method for accept and return initial Matrix
	public static int[][] initialMatrix(int row,int col){
		//Declaration and Initialization
		int[][] matrix = new int[row][col];
		int value = 0;
		
		//Store elements to MxN Matrix
		for(int i=0;i<row;i++){
			for(int j=0;j<col;j++){
				matrix[i][j] = ++value;
			}
		}
		
		//return
		return matrix;
	}
}