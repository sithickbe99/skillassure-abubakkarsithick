//package
package com.bootcamp.dicoveri.Eurofin;

//imports
import java.util.Scanner;

//Implementation of Linear Search
class LinearSearch{
	//Implementation of main method
	public static void main(String[] args){
		//Program to search an element in array using Linear Search technique.
		//Declaration and Initialization
		int size,arr[],search,location;
		
		//Scanner class object creation
		Scanner sc = new Scanner(System.in);
		
		//Accept the size of Integer array
		System.out.println("Enter the number of integer element :");
		size = sc.nextInt();
		
		//Initialization of integer array for given size
		arr = new int[size];
		
		//Accept the integer values to store an array
		System.out.println("Enter "+size+" integer elements:");
		for(int i=0;i<size;i++){
			arr[i] = sc.nextInt();
		}
		
		//Accept a number to be searched
		System.out.println("Enter key element to be searched:");
		search = sc.nextInt();
		
		//Method call to find an key element position in a array
		location = linearSearch(arr,size,search);
		
		//Display result
		display(location,search);
		
	}
	
	//Method to check an element found or not
	public static int linearSearch(int[] arr,int size, int key){
		for(int i=0;i<size;i++){
			if(arr[i] == key){
				return i;
			}
		}
		return -1;
	}
	
	//Method to display an result
	public static void display(int location,int search){
		if(location!=-1){
			//display if element is found
			System.out.println(search+" element is found at location "+(location+1)+".");
		}else{
			//display if element is not found
			System.out.println(search+" element is not found.");
		}
	}
}