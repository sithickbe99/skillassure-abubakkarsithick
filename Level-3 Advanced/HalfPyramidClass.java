//package
package com.bootcamp.dicoveri.Eurofin;

//imports
import java.util.Scanner;

//Implementation of Half Pyramid class
class HalfPyramidClass{
	//Implementation of main method
	public static void main(String[] args){
		for(int i=0;i<6;i++){
			for(int j=0;j<=i;j++){
				System.out.print("* ");
			}
			System.out.println();
		}				
	}l
}