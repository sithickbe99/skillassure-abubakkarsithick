//package
package com.bootcamp.dicoveri.Eurofin;

//imports
import java.util.Scanner;

//Implementation class of student performence
class ReportCard{	
	//Implementation of main method
	public static void main(String... args){
		//Program to find the student performence based on obtained marks
		//Declaration
		String studentName;
		float subject1,subject2,subject3,totalMark,averageMark;
		
		//Scanner class object creation
		Scanner sc = new Scanner(System.in);
	
		//Accept student details
		System.out.println("Student's Name : ");
		studentName = sc.next();
		System.out.println("Mark of Subject 1 : ");
		subject1 = sc.nextFloat();
		System.out.println("Mark of Subject 2 : ");
		subject2 = sc.nextFloat();
		System.out.println("Mark of Subject 3 : ");
		subject3 = sc.nextFloat();
		
		//Method call to calculate total mark
		totalMark = total(subject1,subject2,subject3);
		
		//Method call to find average mark
		averageMark = average(totalMark,3F);
		
		//display student details
		display(studentName,subject1,subject2,subject3,totalMark,averageMark);
	}
	
	//Method to calculate total
	public static float total(float subject1,float subject2,float subject3){
		//Return
		return subject1+subject2+subject3;
	}
	
	//Method to calculate average
	public static float average(float totalMark,float noOfSubjects){
		//Return
		return totalMark/noOfSubjects;
	}
	
	//display result
	public static void display(String studentName,float subject1,float subject2,float subject3,float totalMark,float averageMark){
		//display student basic details
		System.out.println("***"+studentName+"\'s Report Card***");
		System.out.println("subject 1 : "+subject1);
		System.out.println("subject 2 : "+subject2);
		System.out.println("subject 3 : "+subject3);
		System.out.println("Total : "+totalMark);
		System.out.println("Average : "+averageMark);
		System.out.println("------------------------");
		
		//filter the mark wise performence
		if(subject1>=35 && subject2>=35 && subject3>=35){
 			if(averageMark>=60.0){
				System.out.println("\t1st class");
			}else if(averageMark>=50.0 && averageMark<60.0){
				System.out.println("\t2nd class");
			}else if(averageMark>=35.0 && averageMark<50.0){
				System.out.println("\tpass class");
			}else{
				System.out.println("\tFail");
			}
		}else{
			System.out.println("\tFail");
		}
		
		System.out.println("------------------------");
	}
}