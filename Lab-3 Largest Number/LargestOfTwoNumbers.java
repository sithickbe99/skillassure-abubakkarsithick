//package
package com.bootcamp.dicoveri.Eurofin;

//imports
import java.util.Scanner;

class LargestOfTwoNumbers{
	//Implementation of main method
	public static void main(String[] args){
		//Program to find largest of two numbers.
		//Declaration
		int number1,number2;
		
		//Accept and store an number1 and number2
		Scanner sc = new Scanner(System.in);
		number1 = sc.nextInt();
		number2 = sc.nextInt();
		
		//Find largest number between number1 and number2
		System.out.print("Largest among "+number1+" & "+number2+" is: ");
		if(number1 > number2){
			System.out.print(number1);
		}else{
			System.out.print(number2);
		}		
	}
}