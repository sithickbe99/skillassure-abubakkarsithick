//package
package com.bootcamp.dicoveri.Eurofin;

//imports
import java.util.Scanner;

//Implementation of Fibonacci series class
class FibonacciSeries{
	//Implementation of main method
	public static void main(String[] args){
		//Declaration
		int range;
		
		//Scanner class object creation
		Scanner sc = new Scanner(System.in);
		
		//Accept the Number from User
		range = sc.nextInt();
		
		//Method call to print fibonacci series
		fibonacci(range);
	}
	
	//Method to implement for display fibonacci series on given renges
	public static void fibonacci(int range){
		if(range>0){
			//Declaration
			int number1,number2,next;
		
			//Initialization
			number1 = 0;
			number2 = 1;
			
			if(range<=2){
				if(range==1){
					//disply only one number when range is 1.
					System.out.println(number1);
				}else{
					//disply two numbers when range is 2.
					System.out.println(number1+" "+number2);
				}
			}else{
				//W.K.T the first two values
				System.out.print(number1+" "+number2);
				for(int i=3;i<=range;i++){
					next = number1+number2;
					System.out.print(" "+next);
					number1 = number2;
					number2 = next;
				}
			}
		}else{
			//display No output when range is less than 0.
			System.out.println("No Output");
		}
	}
}