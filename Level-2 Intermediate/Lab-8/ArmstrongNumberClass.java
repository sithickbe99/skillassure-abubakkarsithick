//package
package com.bootcamp.dicoveri.Eurofin;

//imports
import java.util.Scanner;

//Implements the ArmstrongNumber class
class ArmstrongNumberClass{
	//Implementation of main method
	public static void main(String[] args){
		//Program to check Armstrong Number
		//Declaration
		int number;
		
		//Initialization
		number = 0;
		
		//Scanner class object creation
		Scanner sc = new Scanner(System.in);
		
		//Accept Number by user
		number = sc.nextInt();
		
		//Method call to check given specific number is armstrong or not
		boolean result = isArmstrongNumber(number);
		
		//Display result
		if(result){
			System.out.println(number+" is an Armstrong Number.");
		}else{
			System.out.println(number+" is not an Armstrong Number.");
		}
		
	}
	
	//Method to check given number is armstrong or not
	public static boolean isArmstrongNumber(int number){
		//Declaration
		int remainder,total,temp;
		
		//Initialization
		remainder = 0;
		total = 0;
		temp = number;
		
		//Caculation
		while(temp > 0){
			remainder = temp % 10;
			total += (remainder*remainder*remainder);
			temp /= 10;
		}
		
		//check given number is equal to armstrong number
		if(total == number){
			return true;
		}
		
		//Return
		return false;
	}
}