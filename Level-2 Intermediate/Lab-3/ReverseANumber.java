//package
package com.bootcamp.dicoveri.Eurofin;

//imports
import java.util.Scanner;

//Implementation of ReverseANumber class
class ReverseANumber{
	//Main method implementation
	public static void main(String[] args){
		//Program to perform given number to reverse order.
		//Declaration
		int number,reverse,remainder;
		
		//Initialization
		number = 0;
		remainder = 0;
		reverse = 0;
		
		//Create an Scanner Object
		Scanner sc = new Scanner(System.in);
		
		//Accept an number
		System.out.println("Input your number and press enter: ");
		number = sc.nextInt();
		
		//Reverse an number by using while loop
		while(number > 0){
			//Get last digit of an number
			remainder = number%10;
			
			//store an digits one by one
			reverse = (reverse*10)+remainder;
			
			//store remaining digits of number except last digit
			number /= 10;
		}
		
		//Display result
		System.out.println("Reverse of input number is: "+reverse);
	}
}