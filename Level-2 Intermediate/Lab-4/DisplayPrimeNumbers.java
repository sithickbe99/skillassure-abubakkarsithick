//package
package com.bootcamp.dicoveri.Eurofin;

//Implementaion of displayPrimeNumbers class
class DisplayPrimeNumbers{
	//Implementaion of main method
	public static void main(String[] args){
		//Program to display the prime numbers from 1 to 100.
		//Declaration
		int lowerBound;
		int upperBound;
		
		//Initialization
		lowerBound = 1;
		upperBound = 100;
		
		//Method call for display result
		displayPrimeNumbers(lowerBound,upperBound);
	}
	
	//method to implemet whether check given number is prime or not
	public static boolean isPrime(int number){
		//Declaration with Initialization
		boolean flag = true;
		//ignore 0 and 1 because not an prime
		if(number==0 || number==1){
			return !flag;
		}
			
		//for loop
		for(int i=2;i<=number/2;i++){			
			//check if number divided by 'i'
			if(number%i==0){
			    flag = false;
			    break;
			}
		}
		
		//Return
		return flag;
	}
	
	//Implementaion of print the prime numbers in between range
	public static void displayPrimeNumbers(int startPoint, int endPoint){
		//for loop implementation to find prime numbers
		for(int i=startPoint;i<=endPoint;i++){
			//Method call and check whether number is prime or not
			if(isPrime(i)){
				//display the all prime numbers
				System.out.print(i+" ");
			}
		}
	}
}