//package
package com.bootcamp.dicoveri.Eurofin;

//imports
import java.util.Scanner;

//Implementation of AverageOfArray class
class AverageOfArrayClass{
	//Implementation of main method
	public static void main(String... args){
		//Program to find averge of numbers using array.
		//Declaration
		int arr[],sum,size;
		double average;
		
		//Initialization
		sum=0;
		size=0;
		average=0.0;
		
		//Scanner class object creation
		Scanner sc = new Scanner(System.in);
		
		//Accept size of an array
		size = sc.nextInt();
		
		//Initialization of array to given size
		arr = new int[size];
		
		//Accept the array elements from user
		for(int i=0;i<size;i++){
			arr[i] = sc.nextInt();
		}
		
		//Method call to find sum of array elements.
		sum = sum(arr,size);
		
		//Method call to find Average Calculation
		average = average(sum,size);
		
		//Display result
		display(arr,average,sum);
	}
	
	//Method to return sum of array elements
	public static int sum(int[] arr, int size){
		//Declaration and Initialization
		int sum = 0;
		
		//Sum calculation
		for(int i=0;i<size;i++){
			sum += arr[i];
		}
		return sum;
	}
	
	//Method to return average of array
	public static double average(int sum, int size){
		//Return Average
		return sum/(double)size;
	}
	
	//Method to display result
	public static void display(int[] arr, double average,int sum){
		System.out.print("Array integers:");
		for(int i=0;i<arr.length;i++){
			System.out.print(" "+arr[i]);
		}
		System.out.println("\nSum of array numbers is : "+sum);
		System.out.println("Average of array numbers is : "+average);
	}
}