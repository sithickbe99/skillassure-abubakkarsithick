package com.bootcamp.dicoveri.Eurofin;

import java.util.Scanner;
import java.util.Arrays;
import java.util.ArrayList;

class BinarySearch{
	//Hold the duplicate values of given elements
	public static ArrayList<Integer> duplicate = new ArrayList<>();
	
	//Main method implementation
	static public void main(String[] args){
		//Program to perform binary search on a list of integer numbers.
		//Declarations
		int noOfElement,key,foundIndex;
		int[] arr;
		
		//Initialization
		noOfElement = 0;
		key = 0;
		foundIndex = -1;
		
		//Create an Scanner class object
		Scanner sc = new Scanner(System.in);
		
		//Accept the size of an integer array by using scanner object.
		System.out.println("Enter number of elements: ");
		noOfElement = sc.nextInt();
		
		//Initialization for given size of array.
		arr = new int[noOfElement];
		
		//Accept values from console
		System.out.println("Enter "+noOfElement+" integers");
		for(int i=0;i<noOfElement;i++){
			//store an values to integer arr
			arr[i] = sc.nextInt();
			duplicate.add(arr[i]);
		}
		
		//Accept the key element from console
		System.out.println("Enter the search value: ");
		key = sc.nextInt();
		
		//Sort an array's element by natural ascending order
		Arrays.sort(arr);
		
		//Method call to find key element present position
		foundIndex = binarySearch(arr,0,noOfElement-1,key);
		
		//Method call to display the result
		displayResult(foundIndex,key);
	}
	
	//Method implementation for display the result.
	public static void displayResult(int location,int key){
		if(location!=-1){
			//display key value with present location
			System.out.println(key+" found at location "+(duplicate.indexOf(key)+1)+".");
		}else{
			//display key value not present
			System.out.println(key+" not found.");
		}
	}
	
	//Method Implementation for binary search technique.
	public static int binarySearch(int[] arr, int first, int last, int key){
		//Declarations
		int mid;
		
		//Initialization
		mid = 0;
		
		while(first <= last){
			//find the mid position of an arry
			mid = first + (last-first)/2;
			
			//Searching an key element
			if(arr[mid] == key){
				return mid;
			}else if(arr[mid] < key){
				first = mid + 1;
			}else{
				last = mid - 1;
			}
		}
		
		//Return
		return -1;
	}
}