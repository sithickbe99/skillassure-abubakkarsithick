//package
package com.bootcamp.dicoveri.Eurofin;

//Implement the Smallest number class
class SmallestNumberAmongThreeClass{
	//Implementation of main method
	public static void main(String... args){
		//Program to find smallest number among three number using ternary oprator
		//Declarations
		int number1,number2,number3,smallest;
		
		//Initialization
		number1 = 0;
		number2 = 0;
		number3 = 0;
		smallest = 0;
		
		//Accept values from commandLine arguments
		number1 = Integer.parseInt(args[0]);
		number2 = Integer.parseInt(args[1]);
		number3 = Integer.parseInt(args[2]);
		
		//method call to find smallest number among three numbers
		smallest = smallest(number1,number2,number3);
		
		//display smallest number
		System.out.println("Given numbers: "+number1+" "+number2+" "+number3);
		System.out.println("Smallest number is: "+smallest);
		
	}
	
	//method to return smallest number among three numbers
	public static int smallest(int number1, int number2, int number3){
		return (number1<number2 && number1<number3) ? number1 : (number2<number1 && number2<number3) ? number2 : number3;
	}
}