//package
package com.bootcamp.dicoveri.Eurofin;

//imports
import java.util.Scanner;

class PowerOfNumber{
	//Method to implement power of number
	static public int pow(int base, int exp){
		//Declaration with initial value
		int sum = 1;
		
		//Calculate Power of a number
		for(int i=1;i<=exp;i++){
			sum = sum*base;
		}
		
		//Return
		return sum;
	}
	//Implementation of main method
	static public void main(String[] args){
		//Program to find an power of an given number
		//Declaration
		int base,exp,result;
		
		//Accept and Store an value to variables
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Base and Exp of an Number :");
		base = sc.nextInt();
		exp = sc.nextInt();
		
		//Method call to calculate power
		result = pow(base,exp);
		
		//Display result
		System.out.println(result);
	}
}