//package
package com.bootcamp.dicoveri.Eurofin;

//Implementation of Vowel or Consonent class
class VowelOrConsonent{
	//Implementation of main method
	public static void main(String[] args){
		//Program to check Vowel or Consonent using Switch Case Statement
		//Declare and Intialize with value
		String sentance = "HjdkaMeOkIgAYp45@nUS";
		
		//Each a character to check wheter vowels or consonent
		for(int i=0;i<sentance.length();i++){
			//Get an character one by one
			char letter = sentance.charAt(i);
			
			//Method call to check letter is vowel or not
			if(isVowel(letter)){
				System.out.println(letter+" is an Vowel.");
			}else{
				System.out.println(letter+" is an Consonent.");
			}
		}
	}
	
	//Method implementation to check whether letter Vowel or Consonent
	public static boolean isVowel(char letter){
		//Switch case Statement
		switch(letter){
				case 'a':
				case 'e':
				case 'i':
				case 'o':
				case 'u':
				case 'A':
				case 'E':
				case 'I':
				case 'O':
				case 'U':
					return true;
				default:
					return false;
			}
	}
}