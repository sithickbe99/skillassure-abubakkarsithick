//package
package com.bootcamp.dicoveri.Eurofin;

class ExpressionEvaluation{
	//Implementation of main method
	public static void main(String[] args){
		//Program to find the expression value
		//Declaration
		int a,b,c,d,e,result;
		
		//Initialization
		a=2;
		b=2;
		c=1;
		d=3;
		e=10;
		
		//Expression
		result = a + b - c * d / e;
		
		//display the result to console
		System.out.println(result);
	}
}